# TP lora COUAD
Les textes de TP sont disponibles sur le blog du Telefab d'IMT Atlantique Brest.

TP 1 : Démarrer avec Lopy4 Pycom https://telefab.fr/2021/09/27/demarrer-avec-lopy4-pycom/

TP 2 : Aller plus loin avec la Lopy4 https://telefab.fr/2021/10/04/aller-plus-loin-avec-la-lopy4/

TP 3 : Mini-projet capteur météo et LoRaWAN avec la Lopy4 https://telefab.fr/2021/10/18/mini-projet-capteur-meteo-et-lorawan-avec-la-lopy4/

Dans ce dépôt, vous trouvez les exemples de code à utiliser pour réaliser les TPs.

Vous trouverez également le fichier [modele.html](https://gitlab.imt-atlantique.fr/clanglai/tp-lora-cooc/-/blob/main/modele.html?ref_type=heads) qui est un template de rédaction d'un article de blog en html pour le wordpress du blog du Telefab.