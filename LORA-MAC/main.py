from network import LoRa
import ubinascii

# lora = LoRa()
lora = LoRa(mode=LoRa.LORA, region=LoRa.EU868)
print("DevEUI: %s" % (ubinascii.hexlify(lora.mac()).decode('ascii')))
#print("DeviceEui Hex Payload " + ".join({:02x}".format(x) for x in lora.mac()))
