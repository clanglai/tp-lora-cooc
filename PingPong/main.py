# main.py -- put your code here!
from network import LoRa
import socket
import time

# Please pick the region that matches where you are using the device

lora = LoRa(mode=LoRa.LORA, region=LoRa.EU868, adr=False)
print('Coding rate {}'.format(lora.coding_rate()))

#lora.sf(12)
print('SF {}'.format(lora.sf()))

s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

s.setblocking(False)

# Code pour le Pong ; réception puis émission
i = 0
while True:
    if s.recv(64) == b'Ping':
        print('je recois Ping du Node A')
        print(lora.stats())
        #print(lora.coding_rate())
        s.send('Pong')
        print('Pong {}'.format(i))
        i = i+1
    time.sleep(5)

# Code pour le Ping ; uniquement émission
# msg = 'Ping'
# #msg = 'Ping hello i m devui' + (ubinascii.hexlify(lora.mac()).decode('ascii'))
# #print(msg)
# i = 0
# while True:
#     s.send(msg)
#     print('Ping {}'.format(i))
#     print(lora.stats())
#     i= i+1
#     time.sleep(5)

