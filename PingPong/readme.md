# LoPy to LoPy
This example shows how to connect two Pycom LoRa capable modules (nodes) via raw LoRa. Node A will continuously send a packet containing Ping. Once Node B receives such a packet, it will respond with Pong. You will see the count messages appear in the REPL. You can adapt this example to have mutual communication between two LoRa nodes.

Extracted from https://docs.pycom.io/tutorials/networks/lora/module-module/

