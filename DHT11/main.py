import time
import pycom
import ubinascii
from machine import enable_irq, disable_irq, Pin
from dht import DTH


#Mettre votre n° de broche
th = DTH('P9')
pycom.heartbeat(False)

while True:
    result = th.read()
    while not result.is_valid():
        time.sleep(.5)
        result = th.read()
    print('Temp:', result.temperature)
    print('RH:', result.humidity)

    if result.temperature > 25:
        pycom.rgbled(0xff0000)
    else:
        pycom.rgbled(0x00ff00)

    #5 secondes entre 2 lectures avec retour au bleu
    time.sleep(2.5)
    pycom.rgbled(0x0000ff)
    time.sleep(2.5)
