from network import LoRa
import socket
import time
import struct
import ubinascii
import binascii
import pycom

def connect_to_ttn(lora_object):
    """Receives a lora object and tries to join"""
    # join a network using OTAA (Over the Air Activation),
    # choose dr = 0 if the gateway is not close to your device, try different dr if needed
    lora_object.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0, dr=0)
    # wait until the module has joined the network
    pycom.rgbled(0x7f7f00) #yellow
    while not lora_object.has_joined():
        time.sleep(2.5)
        print('Not yet joined...')
        lora.nvram_erase()
        

pycom.heartbeat(False)

app_eui = ubinascii.unhexlify('0000000000000000')
app_key = ubinascii.unhexlify('----------------') # replace the dash by the AppKey provided by TTN
#uncomment to use LoRaWAN application provided dev_eui
dev_eui = ubinascii.unhexlify('-----------------') # replace the dash by the DevEUI provided to TTN

pycom.rgbled(0xff0000) #red
time.sleep(1)


# configure the parameters of LoRaWAN, authorize adaptive datarate and choose a CLASS C device
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868, device_class=LoRa.CLASS_C, adr=False)
print('DevEUI : ', binascii.hexlify(lora.mac()).upper())
lora.nvram_restore() #if there is nothing to restore it will return a False

connect_to_ttn(lora)

print("CONNECTED!!")
pycom.rgbled(0x00ff00) #green


# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 0)

# make the socket blocking
s.setblocking(True)

for i in range (20):
    pkt = b'PKT ' + bytes([i])
    print('Sending:', pkt)
    s.send(pkt)
    time.sleep(4)
    # without downlink transmission first
    #rx, port = s.recvfrom(256)
    #if rx:
    #    print('Received: {}, on port: {}'.format(rx, port))
    #time.sleep(6)
